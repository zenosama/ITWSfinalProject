from flask import Flask, render_template, request, flash, redirect, url_for, session, logging
# from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
import sqlite3
import random


class RegiserForm(Form):
    m1_first = StringField('First Name of Half', [validators.Length(min=1, max=20)])
    m1_last = StringField('Last Name of Half', [validators.Length(min=1, max=20)])
    m1_email = StringField('Email of Half', [validators.Length(min=2, max=100)])

    m2_first = StringField('First Name of Other Half', [validators.Length(min=1, max=20)])
    m2_last = StringField('Last Name of Other Half', [validators.Length(min=1, max=20)])
    m2_email = StringField('Email of Other Half', [validators.Length(min=2, max=100)])

    # m1_username = StringField('Username', [validators.Length(min=2, max=30)])
    # password = PasswordField('Password', [
    #     validators.DataRequired(),
    #     validators.EqualTo('confirm', message = 'Passwords do not match')
    # ])
    # confirm = PasswordField('Confirm Password')

app = Flask(__name__)

# conn = sqlite3.connect('users.db')
#
# Cursor = conn.cursor()
#
# Cursor.execute("""CREATE TABLE users (
#     id AUTO_INCREMENT PRIMARY KEY,
#     first VARCHAR(20),
#     last VARCHAR(20),
#     email VARCHAR(100),
#     username VARCHAR(30) UNIQUE,
#     password VARCHAR(100),
#     register_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
#     )""")


def storeUser(m1_first, m1_last, m1_email, m2_first, m2_last, m2_email):

    con = sqlite3.connect('database.db')
    cur = con.cursor()

    # cur.execute("SELECT * FROM users WHERE m1_='"+username+"'")
    # if not cur.fetchone():
    otp1 = random.randint(1000,1000000)
    otp2 = random.randint(1000,1000000)

    cur.execute("INSERT INTO users(first, last, email, otp) VALUES(?, ?, ?, ?)", (m1_first, m1_last, m1_email, otp1))
    con.commit()
    cur.execute("INSERT INTO users(first, last, email, otp) VALUES(?, ?, ?, ?)", (m2_first, m2_last, m2_email, otp2))
    con.commit()

    con.close()

    return True


@app.route('/')
@app.route("/index")
def index():
	return render_template('layout.html')


@app.route("/register", methods=["POST","GET"])
def register():
    form = RegiserForm(request.form)
    if request.method == 'POST' and form.validate():

        m1_first = form.m1_first.data
        m1_last = form.m1_last.data
        m1_email = form.m1_email.data

        m2_first = form.m2_first.data
        m2_last = form.m2_last.data
        m2_email = form.m2_email.data


        # username = form.username.data
        # password = (form.password.data)

        if storeUser(m1_first, m1_last, m1_email, m2_first, m2_last, m2_email):
            flash('Visit the link sent on your email to complete your registeration', 'success')
            return redirect(url_for('index'))
        else:
            flash('This email is already in use', 'warning')
        return render_template('register.html', form = form)
    return render_template('register.html', form = form)

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/everify')
def everify():
    return render_template('everify.html')

if __name__ == "__main__":
    app.secret_key="1234"
    app.run(debug=True)
