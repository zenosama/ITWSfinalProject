drop table if exists users;
CREATE TABLE  users (
  id AUTO_INCREMENT PRIMARY KEY,
    first VARCHAR(20),
    last VARCHAR(20),
    email VARCHAR(100) UNIQUE,
    username VARCHAR(30) UNIQUE,
    password VARCHAR(100),
    link VARCHAR(200),
    otp VARCHAR(20),
    register_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
